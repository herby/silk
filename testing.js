define([
    './deploy',
    'amber/core/SUnit',
    // --- packages used only during automated testing begin here ---
    'silk/Silk-Tests'
    // --- packages used only during automated testing end here ---
], function (amber) {
    return amber;
});
