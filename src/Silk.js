define(["amber/boot", "require", "amber/core/Kernel-Collections", "amber/core/Kernel-Infrastructure", "amber/core/Kernel-Methods", "amber/core/Kernel-Objects", "domite/DOMite"], function($boot,requirejs){"use strict";
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
var $pkg = $core.addPackage("Silk");
$pkg.innerEval = function (expr) { return eval(expr); };
$pkg.transport = {"type":"amd","amdNamespace":"silk"};

$core.addClass("Silk", $globals.Domite, [], "Silk");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.Silk.comment="I am subclass of `Domite` with more convenient high-level API.\x0a\x0a##Rendering\x0a\x0a - `aSilk << anObject` uses double-dispatch via `renderOnSilk:`.\x0aThis allows to create widgets\x0a(no formal superclass, anything with `renderOnSilk:` is a widget),\x0aas well as incorporating magic on certain types of objects:\x0a   - blocks: `aSilk << aBlock` runs the block, passing aSilk as a parameter.\x0a   - associations: `aSilk << (key -> value)` set attribute key to value.\x0a\x0aWorthful to note is, rendering a collection has its magic\x0aalready built-in (via `putOn:`) -- if you `stream << aCollection`,\x0aits items are `<<`'d in sequence.\x0aSo, de facto, arrays are deeply flattened when put on a stream via `<<`.\x0a\x0a##Convenience\x0a\x0a - `aCssSelectorString asSilk` returns Silk wrapping an element at a selector.\x0a - `anObject inSilk` returns anObject rendered in a document fragment.\x0a\x0a##Element creation\x0a\x0aThese messages use DNU to dynamically create\x0aelements with any (letters-and-numbers) tag name,\x0aNext samples show this on an example of `<div>`.\x0a\x0a - `Silk DIV` is shortcut for `Silk newElement: 'div'`.\x0a - `aSilk DIV` is shortcut for\x0a`[ |tmp| tmp := Silk DIV. aSilk << tmp. tmp] value`.\x0aIOW, it not just creates the element and returns it,\x0abut also puts in on aSilk.\x0a - `aSilk DIV: anObject` is shortcut for\x0a`aSilk DIV << anObject; yourself`.\x0aIOW, it not just creates and inserts the element,\x0abut puts a content into it.\x0a\x0a##Conclusions\x0a\x0aTaken all this together, one can do pretty neat constructs:\x0a\x0a```\x0a  aSilk P: { 'id'->'mission'. 'We are the champions.' }\x0a```\x0a\x0aadds `<p id=\x22mission\x22>We are the champions.</p>` into `aSilk`\x0aand returns the Silk-wrapped `<p>` with insertion cursor at the end.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "namespace",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "namespace\x0a\x09\x22<String>\x0a\x09XML namespace for elements: html.\x0a\x09The default for all virtual Silk tag messages\x22\x0a\x09\x0a\x09^ self element namespaceURI",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["namespaceURI", "element"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._element())._namespaceURI();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"namespace",{})});
//>>excludeEnd("ctx");
}; }),
$globals.Silk);

$core.addMethod(
$core.method({
selector: "newElement:xmlns:",
protocol: "writing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aString", "anotherString"],
source: "newElement: aString xmlns: anotherString\x0a\x09| el |\x0a\x09\x0a\x09el := self class newElement: aString xmlns: anotherString.\x0a\x09self << el.\x0a\x09^ el",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["newElement:xmlns:", "class", "<<"]
}, function ($methodClass){ return function (aString,anotherString){
var self=this,$self=this;
var el;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
el=$recv($self._class())._newElement_xmlns_(aString,anotherString);
$self.__lt_lt(el);
return el;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"newElement:xmlns:",{aString:aString,anotherString:anotherString,el:el})});
//>>excludeEnd("ctx");
}; }),
$globals.Silk);

$core.addMethod(
$core.method({
selector: "nextPut:",
protocol: "writing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "nextPut: anObject\x0a\x09\x22Double-dispatches anObject via renderOnSilk: message.\x0a\x09If a message returns nil, this fallbacks to superclass.\x0a\x09Otherwise, it is assumed renderOnSilk: did its job.\x22\x0a\x0a\x09(anObject renderOnSilk: self)\x0a\x09\x09ifNil: [ super nextPut: anObject ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "renderOnSilk:", "nextPut:"]
}, function ($methodClass){ return function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(anObject)._renderOnSilk_(self);
if($1 == null || $1.a$nil){
[(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._nextPut_.call($self,anObject))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
} else {
$1;
}
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"nextPut:",{anObject:anObject})});
//>>excludeEnd("ctx");
}; }),
$globals.Silk);


$core.addMethod(
$core.method({
selector: "htmlNamespace",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "htmlNamespace\x0a\x09\x22<String>\x0a\x09XML namespace for HTML elements.\x0a\x09The default for all virtual Silk tag messages\x22\x0a\x09\x0a\x09^ 'http://www.w3.org/1999/xhtml'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (){
var self=this,$self=this;
return "http://www.w3.org/1999/xhtml";

}; }),
$globals.Silk.a$cls);

$core.addMethod(
$core.method({
selector: "namespace",
protocol: "accessing",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "namespace\x0a\x09\x22<String>\x0a\x09XML namespace for elements: html.\x0a\x09The default for all virtual Silk tag messages\x22\x0a\x09\x0a\x09^ self htmlNamespace",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["htmlNamespace"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $self._htmlNamespace();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"namespace",{})});
//>>excludeEnd("ctx");
}; }),
$globals.Silk.a$cls);


$core.addTrait("TSilkBuilder", "Silk");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.TSilkBuilder.comment="I contain Silk's \x22build element via DNU\x22 behaviour.\x0a\x0aI expect #namespace and #newElement:xmlns: to be implemented.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "doesNotUnderstand:",
protocol: "message handling",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aMessage"],
source: "doesNotUnderstand: aMessage\x0a\x09\x22`self DIV` creates (and optionally inserts) a div element.\x0a\x09`aSilk DIV: anObject` creates (and optionally inserts)\x0a\x09a div element, and puts contents in it\x22\x0a\x09^ (self tryMakeDnuElement: aMessage)\x0a\x09\x09ifNil: [ super doesNotUnderstand: aMessage ]",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["ifNil:", "tryMakeDnuElement:", "doesNotUnderstand:"]
}, function ($methodClass){ return function (aMessage){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$self._tryMakeDnuElement_(aMessage);
if($1 == null || $1.a$nil){
return [(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($methodClass.superclass||$boot.nilAsClass).fn.prototype._doesNotUnderstand_.call($self,aMessage))
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.supercall = false
//>>excludeEnd("ctx");
][0];
} else {
return $1;
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"doesNotUnderstand:",{aMessage:aMessage})});
//>>excludeEnd("ctx");
}; }),
$globals.TSilkBuilder);

$core.addMethod(
$core.method({
selector: "newSvgElement",
protocol: "convenience",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "newSvgElement\x0a\x09^ self newElement: 'svg' xmlns: 'http://www.w3.org/2000/svg'",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["newElement:xmlns:"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $self._newElement_xmlns_("svg","http://www.w3.org/2000/svg");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"newSvgElement",{})});
//>>excludeEnd("ctx");
}; }),
$globals.TSilkBuilder);

$core.addMethod(
$core.method({
selector: "tryMakeDnuElement:",
protocol: "instance creation",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aMessage"],
source: "tryMakeDnuElement: aMessage\x0a\x09\x22`DIV` creates a div element.\x0a\x09`DIV: anObject` creates a div element and puts contents in it.\x0a\x09An element can be optionally inserted by #newElement:xmlns:.\x0a\x09When self is an instance and not the class Silk, \x0a\x09then the instance's namespace is used for the new element.\x0a\x09You can do:\x0a\x09\x09svg := Silk newElement: 'svg' xmlns: 'http://www.w3.org/2000/svg'.\x0a\x09\x09svg CIRCLE: {'cx' -> 60. 'cy' -> 25. 'r' -> 10}.\x0a\x09This creates a svg circle, not a html circle.\x22\x0a\x09\x0a\x09| selector newElement useArg |\x0a\x09selector := aMessage selector.\x0a\x09selector asUppercase = selector\x0a\x09\x09ifFalse: [ ^ nil ].\x0a\x09selector last = ':'\x0a\x09\x09ifTrue: [ useArg := true. selector := selector allButLast ]\x0a\x09\x09ifFalse: [ useArg := false ].\x0a\x09(selector includes: ':')\x0a\x09\x09ifTrue: [ ^ nil ].\x0a\x09newElement := self newElement: selector asLowercase xmlns: self namespace.\x0a\x09useArg ifTrue: [ newElement << aMessage arguments first ].\x0a\x09^ newElement",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["selector", "ifFalse:", "=", "asUppercase", "ifTrue:ifFalse:", "last", "allButLast", "ifTrue:", "includes:", "newElement:xmlns:", "asLowercase", "namespace", "<<", "first", "arguments"]
}, function ($methodClass){ return function (aMessage){
var self=this,$self=this;
var selector,newElement,useArg;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
selector=$recv(aMessage)._selector();
if(!$core.assert([$recv($recv(selector)._asUppercase()).__eq(selector)
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
,$ctx1.sendIdx["="]=1
//>>excludeEnd("ctx");
][0])){
return nil;
}
if($core.assert($recv($recv(selector)._last()).__eq(":"))){
useArg=true;
selector=$recv(selector)._allButLast();
selector;
} else {
useArg=false;
useArg;
}
if($core.assert($recv(selector)._includes_(":"))){
return nil;
}
newElement=$self._newElement_xmlns_($recv(selector)._asLowercase(),$self._namespace());
if($core.assert(useArg)){
$recv(newElement).__lt_lt($recv($recv(aMessage)._arguments())._first());
}
return newElement;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"tryMakeDnuElement:",{aMessage:aMessage,selector:selector,newElement:newElement,useArg:useArg})});
//>>excludeEnd("ctx");
}; }),
$globals.TSilkBuilder);

$core.setTraitComposition([{trait: $globals.TSilkBuilder}], $globals.Silk);
$core.setTraitComposition([{trait: $globals.TSilkBuilder}], $globals.Silk.a$cls);

$core.addMethod(
$core.method({
selector: "renderOnSilk:",
protocol: "*Silk",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aSilk"],
source: "renderOnSilk: aSilk\x0a\x09key attrPut: value on: aSilk",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["attrPut:on:"]
}, function ($methodClass){ return function (aSilk){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self.key)._attrPut_on_($self.value,aSilk);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderOnSilk:",{aSilk:aSilk})});
//>>excludeEnd("ctx");
}; }),
$globals.Association);

$core.addMethod(
$core.method({
selector: "renderOnSilk:",
protocol: "*Silk",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aSilk"],
source: "renderOnSilk: aSilk\x0a\x09self value: aSilk",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["value:"]
}, function ($methodClass){ return function (aSilk){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self._value_(aSilk);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"renderOnSilk:",{aSilk:aSilk})});
//>>excludeEnd("ctx");
}; }),
$globals.BlockClosure);

$core.addMethod(
$core.method({
selector: "inSilk",
protocol: "*Silk",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inSilk\x0a\x09^ Silk newStream << self; yourself",
referencedClasses: ["Silk"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["<<", "newStream", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.Silk)._newStream();
$recv($1).__lt_lt(self);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inSilk",{})});
//>>excludeEnd("ctx");
}; }),
$globals.JSObjectProxy);

$core.addMethod(
$core.method({
selector: "renderOnSilk:",
protocol: "*Silk",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aSilk"],
source: "renderOnSilk: aSilk\x0a\x09^ nil",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aSilk){
var self=this,$self=this;
return nil;

}; }),
$globals.JSObjectProxy);

$core.addMethod(
$core.method({
selector: "inSilk",
protocol: "*Silk",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "inSilk\x0a\x09^ Silk newStream << self; yourself",
referencedClasses: ["Silk"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["<<", "newStream", "yourself"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv($globals.Silk)._newStream();
$recv($1).__lt_lt(self);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inSilk",{})});
//>>excludeEnd("ctx");
}; }),
$globals.Object);

$core.addMethod(
$core.method({
selector: "renderOnSilk:",
protocol: "*Silk",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aSilk"],
source: "renderOnSilk: aSilk\x0a\x09^ nil",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: []
}, function ($methodClass){ return function (aSilk){
var self=this,$self=this;
return nil;

}; }),
$globals.Object);

$core.addMethod(
$core.method({
selector: "asSilk",
protocol: "*Silk",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "asSilk\x0a\x09^ Silk at: self asString",
referencedClasses: ["Silk"],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["at:", "asString"]
}, function ($methodClass){ return function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.Silk)._at_($self._asString());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"asSilk",{})});
//>>excludeEnd("ctx");
}; }),
$globals.String);

$core.addMethod(
$core.method({
selector: "attrPut:on:",
protocol: "*Silk",
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject", "aSilk"],
source: "attrPut: anObject on: aSilk\x0a\x09aSilk attrAt: self put: anObject",
referencedClasses: [],
//>>excludeEnd("ide");
pragmas: [],
messageSends: ["attrAt:put:"]
}, function ($methodClass){ return function (anObject,aSilk){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv(aSilk)._attrAt_put_(self,anObject);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"attrPut:on:",{anObject:anObject,aSilk:aSilk})});
//>>excludeEnd("ctx");
}; }),
$globals.String);

});
